# Tic-Tac-Toe in Prolog

A simple program that can play tic-tac-toe optimally using
Prolog. This was written a few years ago as an exercise for learning
the language.

If you'd like to run the code, clone this repository and run

    swipl tic-tac-toe.pl
    tic-tac-toe(p).

in the root folder, where `p` is what turn you would like to take (`1`
is first, `2` is second). The game should describe how to move.

It was meant to be an implementation of game tree search (without
alpha/beta pruning) but I think there may be one more bug affecting
the search component.
