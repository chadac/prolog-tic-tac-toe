tic_tac_toe(T) :-
    % init_board(B),
    % write_board(B),
    write('Welcome! During your turn, enter a row-indexed number 0-8 in the format #.'),
    init_board(B),
    write_board(B),
    write('\n'),
    turn(B,T).

turn(B, T) :-
    ((T==1,
      player_move(B,B1,1,2)
      %% computer_move(B,B1,1,2)
     );
     (T==2,
      computer_move(B,B1,2,1)
    )),
    write_board(B1), nl,
    (find_win(B1, W) ->
   (W is 1 ->
        write('Player 1 wins!');
    write('Player 2 wins!'));
     (num_empty_spaces(B1,0) ->
    write('Draw! You both suck at this game, don\'t even try again.');
      (T is 1 ->
     turn(B1, 2);
       turn(B1, 1)))).

player_move(B, C, P1, P2) :-
    write('Player move: '),
    read(I),
    alter_board(B,I,P1,C).

computer_move(B, C, P1, P2) :-
    minimax_board(min, B, C, F, P2, P1),
    write(F).

%% Moving up one level in the tree
minimax_switch(min, max).
minimax_switch(max, min).

%% The actual algorithm
minimax(min, [], [], 100,_,_).
minimax(max, [], [], -100, _,_).
minimax(Turn, [B|Boards], Solution, F, P1, P2) :-
    minimax_board(Turn, B, _, F2, P1, P2),
    (minimax(Turn, Boards, Sol1, F1, P1, P2) ->
   ((Turn = min, F1 < F2; Turn = max, F1 > F2) ->
        Solution = Sol1, F is F1
    ;
    Solution = B, F is F2
   );
     (Solution = B, F is F2)).

%% Generates new boards
minimax_board(Turn, Board, Solution, F, P1, P2) :-
    (num_empty_spaces(Board, 0); find_win(Board, _)) ->
  (fitness(Board, F), Solution = Board);
    (minimax_switch(Turn, T1),
     moves(Board, Board, 0, BS, P2),
     minimax(T1, BS, Solution, F, P2, P1)).

%% Valuation of board configuration
fitness(Board, 1)    :- find_win(Board, 2).
fitness(Board, -1)   :- find_win(Board, 1).
fitness(Board, 0) :-
    num_empty_spaces(Board, 0).

%% Number of empty spaces on the board
num_empty_spaces([], 0).
num_empty_spaces([0|Board], E) :-
    num_empty_spaces(Board, E1),
    E is E1 +1.
num_empty_spaces([S|Board], E) :-
    S \= 0,
    num_empty_spaces(Board, E).


write_boards([]).
write_boards([B|BS]) :-
    write(B),
    write_board(B), nl, nl,
    write_boards(BS).

test_moves :-
    % init_board(B),
    B = [0,0,0,0,0,0,0,0,0],
    moves(B,B,0,BS,1),
    write(BS),
    write_boards(BS).

moves(_, [], _, [], _).
moves(Board, [0|B], I, BS, P) :-
    I1 is I +1,
    alter_board(Board, I, P, B1),
    moves(Board, B, I1, BS1, P),
    BS = [B1|BS1].
moves(Board, [S|B], I, BS, P) :-
    I1 is I +1, S \= 0,
    moves(Board, B, I1, BS, P).

alter_board([_|T], 0, X, [X|T]).
alter_board([H|T], I, X, [H|R]) :-
    % write(I),
    I > 0,
    I1 is I-1,
    alter_board(T,I1,X,R).

init_board(B) :-
    B = [0,0,0,0,0,0,0,0,0].

find_win([X,X,X,_,_,_,_,_,_],X) :- X \= 0.
find_win([_,_,_,X,X,X,_,_,_],X) :- X \= 0.
find_win([_,_,_,_,_,_,X,X,X],X) :- X \= 0.
find_win([X,_,_,X,_,_,X,_,_],X) :- X \= 0.
find_win([_,X,_,_,X,_,_,X,_],X) :- X \= 0.
find_win([_,_,X,_,_,X,_,_,X],X) :- X \= 0.
find_win([X,_,_,_,X,_,_,_,X],X) :- X \= 0.
find_win([_,_,X,_,X,_,X,_,_],X) :- X \= 0.

write_block(0) :- write('_').
write_block(1) :- write('X').
write_block(2) :- write('O').

write_board([X1,X2,X3,Y1,Y2,Y3,Z1,Z2,Z3]) :-
    write('\n'),
    write_block(X1),
    write(' '),
    write_block(X2),
    write(' '),
    write_block(X3),
    write('\n'),
    write_block(Y1),
    write(' '),
    write_block(Y2),
    write(' '),
    write_block(Y3),
    write('\n'),
    write_block(Z1),
    write(' '),
    write_block(Z2),
    write(' '),
    write_block(Z3).
